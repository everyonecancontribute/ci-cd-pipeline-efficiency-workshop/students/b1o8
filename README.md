# b1o8

Hello, this is your personal workshop project 👋 

I ([@dnsmichi](https://gitlab.com/dnsmichi)) have prepared the workshop with slides and exercises to verify. You can learn and practice async after the workshop. 

You can edit this README with your own notes, copy config snippets, add screenshots, create more Markdown files - just as you like. Tip: For multiple file edits, use the Web IDE.

If you like this workshop, please share and tag me on social: [Twitter](https://twitter.com/dnsmichi) - [LinkedIn](https://www.linkedin.com/in/dnsmichi/)

Please [share workshop feedback](https://osad-munich.org/workshop-feedback-effiziente-devsecops-pipelines-in-einer-cloud-nativen-welt/) with the OSAD organizers - it's only 4 questions. Thanks! :) 

## Resources 

- [Slides with exercises](https://docs.google.com/presentation/d/12ifd_w7G492FHRaS9CXAXOGky20pEQuV-Qox8V4Rq8s/edit)
- [Files including solutions](https://gitlab.com/everyonecancontribute/ci-cd-pipeline-efficiency-workshop/pipeline-efficiency-workshop-exercises)
- [Documentation](https://docs.gitlab.com/)
- [Website](https://about.gitlab.com/)
- [Handbook](https://about.gitlab.com/handbook/)


## My notes 🦊 

Edit this README.md with your own notes throughout the workshop 💡 

> **Tip**: [Markdown Reference](https://docs.gitlab.com/ee/user/markdown.html)

```yaml
this-is-yaml-in-a-code-block:
    script:
        - echo "Hello"
```

<!--
This is a hidden comment.

After the workshop, you can transfer this project into your personal namespace in `Settings > General > Advanced > Transfer project`.
-->
